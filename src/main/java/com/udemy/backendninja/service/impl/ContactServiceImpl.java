package com.udemy.backendninja.service.impl;

import java.util.ArrayList;
import java.util.List;
//import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.udemy.backendninja.converter.ContactConverter;
import com.udemy.backendninja.entity.Contact;
import com.udemy.backendninja.model.ContactModel;
import com.udemy.backendninja.repository.ContactRepository;

@Service("contactServiceImpl")
public class ContactServiceImpl implements ContactService {

	@Autowired
	@Qualifier("contactRepository")
	private ContactRepository contactRepository;

	@Autowired
	@Qualifier("contactConverter")
	private ContactConverter contactConverter;

	@Override
	public ContactModel addContact(ContactModel contactModel) {
		Contact contact = contactRepository.save(contactConverter.toEntity(contactModel));
		return contactConverter.toModel(contact);
	}

//	@PreAuthorize("permitAll()")
	@Override
	public List<ContactModel> listAllContacts() {
		List<Contact> contacts = contactRepository.findAll();
		List<ContactModel> contactsModel = new ArrayList<ContactModel>();
		for (Contact contact : contacts) {
			contactsModel.add(contactConverter.toModel(contact));
		}
		return contactsModel;

		// Stream API and Lambda Expression
		// return contactRepository.findAll().stream().map(ce ->
		// contactConverter.toModel(ce)).collect(Collectors.toList());
	}

	@Override
	public Contact findContactById(int id) {
		return contactRepository.findById(id);
	}

	@Override
	public ContactModel findContactModelById(int id) {
		return contactConverter.toModel(findContactById(id));
	}

	@Override
	public void removeContact(int id) {
		Contact contact = this.findContactById(id);
		if (contact != null) {
			contactRepository.delete(contact);
		}

	}

}
